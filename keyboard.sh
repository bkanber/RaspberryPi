#!/bin/bash

address="CC:C5:0A:1E:38:94"
nyko="9C:54:1C:87:2E:50"

while (sleep 2)
do

	connected=`sudo hidd --show` > /dev/null

	if [[ ! $connected =~ .*${address}.* ]] ; then
		sudo hidd --connect ${address} > /dev/null 2>&1
	fi

done
